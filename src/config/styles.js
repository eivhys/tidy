import {css} from '@emotion/core'

export const font = css`
    text-transform: uppercase;
    font-size: 1em;
    font-weight: bold;
    `

export const button = css`
    cursor: pointer;
    transition: 0.2s all ease-in-out;
  `

export const shape = css`
    border: none;
    border-radius: 25px;
    padding: 10px 20px;
    `

export const primaryColors = css`
    background: lightgreen;
    color: darkslategrey;
    &:hover {
        background: limegreen;
    }
    `
export const defaultColors = css`
    background: ghostwhite;
    color: gray;
    &:hover {
        background: lightgrey;
    }
    `
export const warningColors = css`
    background: lightcoral;
    color: gray;
    &:hover {
        background: limegreen;
    }
    `
export const disabledColors = css`
    background: darkgrey;
    color: lightgrey;
    `