A very simple button.

```jsx
import {Button} from "neat";
<div>
<Button text="Primary" primary={true} />
<Button text="Default" />
<Button text="Disabled" disabled={true} />
</div>
```