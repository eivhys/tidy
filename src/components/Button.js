import React from "react";
import styled from "@emotion/styled"
import {font, primaryColors, defaultColors, disabledColors, shape, button} from 'config/styles'

const Primary = styled.button`
  ${font}
  ${primaryColors}
  ${shape}
  ${button}
`

const Default = styled.button`
  ${font}
  ${defaultColors}
  ${shape}
  ${button}
`

const Disabled = styled.button`
  ${font}
  ${disabledColors}
  ${shape}
  ${button}
`

export default function Button({ text, primary, disabled, onClick }) {
  if (primary) {
    return <Primary onClick={onClick}>{text}</Primary>;
  } else if (disabled) {
    return <Disabled>{text}</Disabled>
  } else {
    return <Default onClick={onClick}>{text}</Default>;
  }
}