import { render, getByText, fireEvent } from "@testing-library/react";
import React from "react";
import Button from "components/Button";

describe("Button", () => {
    test("should display text", () => {
        const { container } = render(<Button text="Render test" />);
        getByText(container, "Render test");
    });

    test("should handle click events", () => {
        const onClickMock = jest.fn()
        const { container } = render(
            <Button text="OnClick test" onClick={onClickMock} />
        )
        const component = container.firstChild
        fireEvent.click(component)
        expect(onClickMock).toBeCalled()
    })

    test("disabled button should not handle click events", () => {
        const onClickMock = jest.fn()
        const { container } = render(
            <Button disabled={true} text="OnClick test" onClick={onClickMock} />
        )
        const component = container.firstChild
        fireEvent.click(component)
        expect(onClickMock).not.toBeCalled()
    })
    
    test("should make text uppercase", () => {
        const { container } = render(<Button text="Render test" />);
        const component = getByText(container, "Render test");
    
        expect(component).toHaveStyleRule("text-transform", "uppercase");
    });
})
